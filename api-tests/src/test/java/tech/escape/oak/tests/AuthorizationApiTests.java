package tech.escape.oak.tests;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import tech.escape.oak.api.ProjectConfig;
import tech.escape.oak.api.requests.documentSets.DocumentSetsRequest;
import tech.escape.oak.api.requests.documentSets.ForeignCitizensItem;
import tech.escape.oak.api.requests.documentSets.RussianRepresentativesItem;
import tech.escape.oak.api.requests.login.LoginRequest;
import tech.escape.oak.api.responses.login.LoginResponse;
import tech.escape.oak.api.services.DevApiService;
import tech.escape.oak.api.services.UserApiService;

import java.util.*;
import java.util.stream.Collectors;

import static tech.escape.oak.api.conditions.Conditions.statusCode;




public class AuthorizationApiTests {

    @BeforeClass
    public void setUp() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());
        faker = new Faker(new Locale(config.locale()));
        RestAssured.baseURI = config.baseURI();
    }

    private final UserApiService userApiService = new UserApiService();
    private final DevApiService devApiService = new DevApiService();

    private Faker faker;
    private String authToken;

    @Test
    public void testLogin() {
        LoginRequest loginPayload = new LoginRequest()
                .email("pp@uacrussia.ru")
                .password("123");
        authToken = userApiService.login(loginPayload)
                .shouldHave(statusCode(200))
                .asPojo(LoginResponse.class)
                .accessToken();
        List<ForeignCitizensItem> foreignCitizensItem = Collections.singletonList(new ForeignCitizensItem()
                .id(37)
                .passportId(27)
                .revisionId(38));
        List<RussianRepresentativesItem> russianRepresentativesItem = Collections.singletonList(new RussianRepresentativesItem()
                .id(33)
                .revisionId(34));
        int[] intCountries = {250}; int intAcceptancePlaces[] = {9};
        List<Integer> countries = Arrays.stream(intCountries).boxed().collect(Collectors.toList());
        List<Integer> acceptancePlaces = Arrays.stream(intAcceptancePlaces).boxed().collect(Collectors.toList());
        DocumentSetsRequest documentSetsPayload = new DocumentSetsRequest()
                .purpose("TEST666")
                .plannedStartDate("1565816400")
                .plannedEndDate("1566594000")
                .countries(countries)
                .foreignCitizens(foreignCitizensItem)
                .russianRepresentatives(russianRepresentativesItem)
                .acceptancePlaces(acceptancePlaces);
        userApiService.documentsSets(documentSetsPayload, authToken)
                .shouldHave(statusCode(201));
    }
}

