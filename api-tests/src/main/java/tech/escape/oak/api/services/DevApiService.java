package tech.escape.oak.api.services;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class DevApiService extends UserApiService {

    private String authToken;

    public String getAuthToken(){
        return this.authToken;
    }

    public String generateStringFromResource(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }


    public void deleteUserFromDb() {

    }

}
