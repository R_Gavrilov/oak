package tech.escape.oak.api.services;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.aeonbits.owner.ConfigFactory;
import tech.escape.oak.api.ProjectConfig;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;



public class ApiService {

    protected RequestSpecification setup() {
        return RestAssured
                .given().contentType(ContentType.JSON)
//                .header("X-Lamb-Device-Platform", "ios")
//                .header("X-Lamb-Device-OS-Version", "12.3")
//                .header("X-Lamb-Device-Family", "iPhone 5")
//                .header("X-Lamb-App-Version", "1.0.0")
//                .header("X-Lamb-App-Build", "1000")
//                .header("X-Lamb-Device-Locale", "ru_RU")
//                .header("X-Lamb-Auth-Token", authToken)
                .filters(getFilters());
    }

    private List<Filter> getFilters() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());
        if (config.logging()) {
            return Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new AllureRestAssured());
        }
        return Collections.singletonList(new AllureRestAssured());
    }

}
