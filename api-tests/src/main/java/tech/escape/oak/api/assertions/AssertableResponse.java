package tech.escape.oak.api.assertions;

import io.qameta.allure.Step;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tech.escape.oak.api.conditions.Condition;

@RequiredArgsConstructor
@Slf4j
public class AssertableResponse {

    private final Response response;

    @Step("api response should have {condition}")
    public AssertableResponse shouldHave(Condition condition) {
        log.info("About to check condition {}", condition);
        condition.check(response);
        System.out.println();
        return this;
    }

    public <T> T asPojo(Class<T> tClass) {
        return response.as(tClass);
    }

    public Response getResponse() {
        return response;
    }





    public Headers headers() {
        return response.getHeaders();
    }

    public ResponseBody body() {
        return response.getBody();
    }

    public int statusCode() {return response.statusCode();}


//    public String responsePrettyPrint() {
//        return response.prettyPrint();
//    }


}
