package tech.escape.oak.api.requests.documentSets;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class DocumentSetsRequest{


	@JsonProperty("purpose")
	private String purpose;

	@JsonProperty("planned_start_date")
	private String plannedStartDate;

	@JsonProperty("planned_end_date")
	private String plannedEndDate;

	@JsonProperty("countries")
	private List<Integer> countries;

	@JsonProperty("foreign_citizens")
	private List<ForeignCitizensItem> foreignCitizens;

	@JsonProperty("russian_representatives")
	private List<RussianRepresentativesItem> russianRepresentatives;

	@JsonProperty("acceptance_places")
	private List<Integer> acceptancePlaces;

}