package tech.escape.oak.api.responses.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class User{

	@JsonProperty("workspace")
	private Workspace workspace;

	@JsonProperty("role")
	private Role role;

	@JsonProperty("person")
	private Person person;

	@JsonProperty("name")
	private Object name;

	@JsonProperty("active")
	private boolean active;

	@JsonProperty("password_changed")
	private boolean passwordChanged;

	@JsonProperty("id")
	private int id;

	@JsonProperty("can_agree_pass_request")
	private boolean canAgreePassRequest;

	@JsonProperty("department")
	private Object department;

	@JsonProperty("email")
	private String email;

	@JsonProperty("person_id")
	private int personId;
}