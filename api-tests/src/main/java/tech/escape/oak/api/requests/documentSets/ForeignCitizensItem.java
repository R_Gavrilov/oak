package tech.escape.oak.api.requests.documentSets;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class ForeignCitizensItem  {

	@JsonProperty("id")
	private int id;

	@JsonProperty("passport_id")
	private int passportId;

	@JsonProperty("revision_id")
	private int revisionId;

}