package tech.escape.oak.api.requests.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class LoginRequest {

	@JsonProperty("password")
	private String password;

	@JsonProperty("email")
	private String email;
}