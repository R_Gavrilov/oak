package tech.escape.oak.api.responses.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class Revision{

	@JsonProperty("surname_en")
	private Object surnameEn;

	@JsonProperty("russian_company_revision_id")
	private int russianCompanyRevisionId;

	@JsonProperty("post_dative_case")
	private String postDativeCase;

	@JsonProperty("middle_name_en")
	private Object middleNameEn;

	@JsonProperty("post_en")
	private Object postEn;

	@JsonProperty("is_individual")
	private boolean isIndividual;

	@JsonProperty("surname_dative_case")
	private String surnameDativeCase;

	@JsonProperty("surname_genitive_case")
	private String surnameGenitiveCase;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("full_name")
	private Object fullName;

	@JsonProperty("post")
	private String post;

	@JsonProperty("birthday_place")
	private Object birthdayPlace;

	@JsonProperty("surname")
	private String surname;

	@JsonProperty("is_foreign")
	private boolean isForeign;

	@JsonProperty("birthday_date")
	private Object birthdayDate;

	@JsonProperty("foreign_company_revision_id")
	private Object foreignCompanyRevisionId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("phone_number")
	private String phoneNumber;

	@JsonProperty("id")
	private int id;

	@JsonProperty("has_middle_name")
	private boolean hasMiddleName;

	@JsonProperty("email")
	private String email;

	@JsonProperty("name_en")
	private Object nameEn;

	@JsonProperty("post_genitive_case")
	private String postGenitiveCase;
}