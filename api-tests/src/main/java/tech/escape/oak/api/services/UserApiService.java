package tech.escape.oak.api.services;

import io.qameta.allure.Step;
import tech.escape.oak.api.assertions.AssertableResponse;
import tech.escape.oak.api.requests.documentSets.DocumentSetsRequest;
import tech.escape.oak.api.requests.login.LoginRequest;

public class UserApiService extends ApiService {


    @Step
    public AssertableResponse login(LoginRequest loginPayload) {
        return new AssertableResponse(setup()
                .body(loginPayload)
                .when()
                .post("/auth/login"));
    }

    @Step
    public AssertableResponse documentsSets(DocumentSetsRequest documentSetsPayload, String authToken) {
        return new AssertableResponse(setup()
                .header("Authorization", "Bearer " + authToken)
                .body(documentSetsPayload)
                .when()
                .post("/document-sets"));
    }


}
