package tech.escape.oak.api.responses.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class Workspace{

	@JsonProperty("main")
	private boolean main;

	@JsonProperty("id")
	private int id;

	@JsonProperty("title")
	private String title;
}