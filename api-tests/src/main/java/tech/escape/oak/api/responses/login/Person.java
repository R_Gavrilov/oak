package tech.escape.oak.api.responses.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class Person{

	@JsonProperty("foreign_company_id")
	private Object foreignCompanyId;

	@JsonProperty("post_en")
	private Object postEn;

	@JsonProperty("surname_dative_case")
	private String surnameDativeCase;

	@JsonProperty("surname_genitive_case")
	private String surnameGenitiveCase;

	@JsonProperty("can_agree_pass_request")
	private boolean canAgreePassRequest;

	@JsonProperty("workspace_id")
	private int workspaceId;

	@JsonProperty("post")
	private String post;

	@JsonProperty("birthday_place")
	private Object birthdayPlace;

	@JsonProperty("surname")
	private String surname;

	@JsonProperty("id")
	private int id;

	@JsonProperty("selected_passport")
	private Object selectedPassport;

	@JsonProperty("has_middle_name")
	private boolean hasMiddleName;

	@JsonProperty("email")
	private String email;

	@JsonProperty("revision_id")
	private int revisionId;

	@JsonProperty("surname_en")
	private Object surnameEn;

	@JsonProperty("post_dative_case")
	private String postDativeCase;

	@JsonProperty("middle_name_en")
	private Object middleNameEn;

	@JsonProperty("is_individual")
	private boolean isIndividual;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("revision")
	private Revision revision;

	@JsonProperty("can_agree_pass_request_failed_deadline")
	private boolean canAgreePassRequestFailedDeadline;

	@JsonProperty("russian_company_id")
	private int russianCompanyId;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("user_id")
	private Object userId;

	@JsonProperty("is_foreign")
	private boolean isForeign;

	@JsonProperty("birthday_date")
	private Object birthdayDate;

	@JsonProperty("can_sign_reception_notice")
	private boolean canSignReceptionNotice;

	@JsonProperty("name")
	private String name;

	@JsonProperty("phone_number")
	private String phoneNumber;

	@JsonProperty("name_en")
	private Object nameEn;

	@JsonProperty("post_genitive_case")
	private String postGenitiveCase;
}