package tech.escape.oak.api;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({"classpath:config.properties"})
public interface ProjectConfig extends Config {

    String env();

//    @Key("{env}.baseURI")
    String baseURI();

    String locale();

    boolean logging();

}
